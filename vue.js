Vue.component('location-picker', {
  props: ['locations'],
  data: () => ({
    selectedCity: null,
    selectedBranch: null,
  }),
  template: `<div class="location-picker">
        <div class="city">
            <span
                v-for="location of locations"
                v-on:click="selectCity(location)"
                v-bind:class="{ active: selectCity && selectCity.id === location.id }"
            >{{ location.label }}</span>
        </div>
        <div v-if="selectedCity" class="branch">
            <span
                v-for="branch of selectedCity.branches"
                href="#"
                v-on:click="selectBranch(branch)"
                v-bind:class="{ active: selectedBranch && selectedBranch.id === branch.id}"
            >{{ branch.label }}</span>
        </div>
      </div>`,
  created: function() {
    this.selectCity(this.locations[0]);
  },
  methods: {
    selectCity: function(city) {
      this.selectedCity = city;
      this.selectedBranch = city.branches[0];
      this.updateLocation();
    },
    selectBranch: function(branch) {
      this.selectedBranch = branch;
      this.updateLocation();

    },
    updateLocation: function () {
      const location = {
        city: this.selectedCity.id,
        branch: this.selectedBranch.id,
      };
      this.$emit('updated', location)
    }
  }
})

Vue.component('kpi-panel', {
  props: ['cards'],
  template: `
    <div class="kpi-panel">
        <div class="card" v-for="card of cards">
            <h6 class="title">{{ card.label }}</h6>
            <p>{{ card.value }}</p>
        </div>
    </div>
    `
});

Vue.component('sales-panel', {
  data: () => ({
    title: 'Umsatz nach Produktgruppen',
    placeholderText: 'Produktgruppe wählen',
    selectedGroup: null,
  }),
  props: ["data"],
  template: `
    <div class="sales-panel">
    <h3 class="panel-header">{{ title }}</h3>
    <select v-model="selectedGroup" v-on:change="loadChart">
        <option disabled :value="null">{{ placeholderText }}</option>
        <option v-for="productGroup in data" :value="productGroup.id">{{ productGroup.label }}</option>
    </select>
    <br>
    <div class="chart">
        <canvas id="salesChart"></canvas>
    </div>
  </div>`,
  methods: {
    loadChart: function () {
      const items = this.data.find((productGroup) => productGroup.id === this.selectedGroup).items;
      const ctx = document.getElementById('salesChart').getContext('2d');
      new Chart(ctx, {
        type: 'horizontalBar',
        data: {
          labels: items.map((item) => item.label),
          datasets: [{
            label: '# of Sales',
            data: items.map((item) => item.value),
          }]
        },
      });
    }
  }
});

Vue.component('orders-panel', {
  data: () => ({
    title: 'Bestellungen im Zeitcluster',
    placeholderText: 'Bestellungstyp wählen',
    selectedType: null
  }),
  props: ['orderTypes'],
  template: `<div class="orders-panel">
      <h3 class="panel-header">{{ title }}</h3>
      <select v-model="selectedType" v-on:change="loadChart">
        <option disabled :value="null">{{ placeholderText }}</option>
        <option v-for="orderType in orderTypes" :value="orderType.id">{{ orderType.label }}</option>
    </select>
      <br>
      <div class="chart">
        <canvas id="ordersChart"></canvas>
      </div>
      
      </div>`,
  methods: {
    loadChart: function () {
      const ctx = document.getElementById('ordersChart').getContext('2d');
      const salesChart = new Chart(ctx, {
        type: 'line',
        data: {
          // labels: items.map((item) => item),
          datasets: [{
            label: '# of Sales',
            data: this.orderTypes.find((productGroup) => productGroup.id === this.selectedType).data
          }]
        },
      });
    }
  }
});


const app = new Vue({
  el: '#app',
  data: () => ({
    title: "Management Cockpit",
    locations: null,
    kpiData: null,
    salesData: null,
    ordersData: null
  }),
  template: `
    <div>
        <header>
            <h1 class="title">{{ title }}</h1>
            <img class="logo" src="img/burger_king_transparent.png">
        </header>
        
        <nav>
            <location-picker
                :locations="locations"
                v-on:updated="locationPicked"
            ></location-picker>
        </nav>

        <main>
            <kpi-panel v-if="kpiData" :cards="kpiData"></kpi-panel>
            
            <div class="grid">
              <sales-panel v-if="salesData" :data="salesData"></sales-panel>
              <orders-panel v-if="ordersData" :orderTypes="ordersData"></orders-panel>
            </div>
            
        </main>
    </div>
    `,
  created: function () {
    // Load Locations form
    const locations = [
      {
        label: 'Zürich',
        id: 'zrh',
        branches: new Array(6).fill(null).map((item, index) => ({
          label: `Filiale ${index + 1}`,
          id: `branch-${index + 1}`
        }))
      },
      {
        label: 'Luzern',
        id: 'luz',
        branches: new Array(3).fill(null).map((item, index) => ({
          label: `Filiale ${index + 1}`,
          id: `branch-${index + 1}`
        }))
      },
      {
        label: 'Bern',
        id: 'brn',
        branches: new Array(4).fill(null).map((item, index) => ({
          label: `Filiale ${index + 1}`,
          id: `branch-${index + 1}`
        }))
      },
      {
        label: 'Tessin',
        id: 'tsn',
        branches: new Array(2).fill(null).map((item, index) => ({
          label: `Filiale ${index + 1}`,
          id: `branch-${index + 1}`
        }))
      },
    ];
    this.locations = locations;
    // `this` points to the vm instance
    // console.log('a is: ' + this.a)
  },
  methods: {
    locationPicked: function (location) {
      this.loadBranchData(location)
    },
    loadBranchData: function (location) {
      // load KPI data form API
      const kpiData = [
        {
          label: 'Umsatz/m2',
          value: 7450,
        },
        {
          label: 'Umsatz/MA',
          value: 1030,
        },
        {
          label: 'ᴓ Bestellung',
          value: 78,
        },
        {
          label: 'Bestellung/Platz',
          value: 9432,
        },
      ];
      this.kpiData = kpiData;
      // load sales data from API
      const salesData = [
        {
          label: 'Burgers',
          id: 'burgers',
          items: [
            {
              label: 'Big Mac',
              value: 230
            },
            {
              label: 'Cheese Burger',
              value: 130
            },
            {
              label: 'Signature Burger',
              value: 80
            },
          ],
        },
        {
          label: 'Getränke',
          id: 'beverages',
          items: [
            {
              label: 'Cola',
              value: 430
            },
            {
              label: 'Sprite',
              value: 310
            },
            {
              label: 'Fanta',
              value: 180
            },
          ],
        },
        {
          label: 'Snacks',
          id: 'snacks',
          items: [
            {
              label: 'Apple Pie',
              value: 330
            },
            {
              label: 'McSundae',
              value: 210
            },
            {
              label: 'Milchshake',
              value: 100
            },
          ],
        },
      ];
      this.salesData = salesData;

      const ordersData = [
        {
          label: 'Kundenbestellungen',
          id: 'customer',
          data:  [{
            x: 10,
            y: 20
          }, {
            x: 15,
            y: 10
          }, {
            x: 20,
            y: 15
          }]
        },
        {
          label: 'Lieferantbestellungen',
          id: 'supplier',
          data:  [{
            x: 10,
            y: 20
          }, {
            x: 15,
            y: 10
          }],
        },
      ];
      this.ordersData = ordersData;

    },
  }
});


